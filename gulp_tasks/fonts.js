const gulp = require('gulp');
const mainBowerFiles = require('main-bower-files');
const filter = require('gulp-filter');
const path = require('path');
const conf = require('../conf/gulp.conf');

gulp.task('fonts', prepareFontsTask(conf.paths.tmp));
gulp.task('fonts:dist', prepareFontsTask(conf.paths.dist));

function prepareFontsTask(dist) {
  return function fonts() {
    return gulp.src(mainBowerFiles())
      .pipe(filter('**/*.{eot,svg,ttf,woff,woff2}'))
      .pipe(gulp.dest(path.join(dist,'/fonts')));

  }
}

