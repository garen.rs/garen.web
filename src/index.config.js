angular
  .module('app')
  .config(function ($httpProvider) {
    $httpProvider.defaults.useXDomain = true;

    $httpProvider.interceptors.push('ApiInterceptorService');
    $httpProvider.interceptors.push('SimplyRetsApiInterceptorService');
  });
