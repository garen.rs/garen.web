'use strict';

angular
  .module('app')
  .controller('IndexController',
    function ($rootScope, $scope, $timeout, CITIES, MAP_SETTINGS, NavigatorGeolocation, toastr, Propriety, googleMapsApiUrl, GeoCoder, $uibModal, TokenService, Search) {
      var vm = this;

      vm.query = {
        limit: 500,
        lastId: 0
      };

      vm.filters = {};

      vm.zoom = MAP_SETTINGS.defaultZoom;

      vm.buy = false;
      vm.rent = false;
      vm.showSearches = false;
      vm.finding = false;

      vm.cities = CITIES;
      vm.proprietiesFounded = [];

      var labels = {
        apartment: 'Apartamento',
        house: 'Casa',
        minprice: 'Precio',
        maxprice: 'Precio',
        minbeds: 'Cuartos',
        maxbeds: 'Cuartos',
        minbaths: 'Baños',
        maxbaths: 'Baños',
        minarea: 'Area',
        maxarea: 'Area'
      };

      var pairs = [
        {
          min: 'minprice',
          max: 'maxprice',
          getValue: function (propriety) {
            return propriety.listPrice;
          }
        },
        {
          min: 'minbeds',
          max: 'maxbeds',
          getValue: function (propriety) {
            return propriety.property.bedrooms;
          }
        },
        {
          min: 'minbaths',
          max: 'maxbaths',
          getValue: function (propriety) {
            return propriety.property.bathsFull + propriety.property.bathsHalf;
          }
        },
        {
          min: 'minarea',
          max: 'maxarea',
          getValue: function (propriety) {
            return propriety.property.area;
          }
        }
      ];

      vm.geoLocation = function () {
        GeoCoder.geocode({address: 'Miami, FL'}) //TODO mover a florida cuando el api este completa.
          .then(function (results) {

            var geo = results.filter(function (result) {
              return result.address_components.some(function (address) {
                return address.types.indexOf('administrative_area_level_1') !== -1 && address.short_name === 'FL';
              });
            });

            if (!!geo.length) {

              vm.location = {
                lat: geo[0].geometry.location.lat()+1,
                lng: geo[0].geometry.location.lng()+1
              };

              vm.zoom = MAP_SETTINGS.defaultZoom-1;

              $timeout(function () {
                vm.location = {
                  lat: geo[0].geometry.location.lat(),
                  lng: geo[0].geometry.location.lng()
                };

                vm.zoom = MAP_SETTINGS.defaultZoom;
              });

            }


          });

      };

      $scope.$watch('state.filters', function () {
        if (vm.filterActive && vm.filterActive.$$state.status === 0) return;

        vm.filterActive = $timeout(function () {
          filterProperties();
        }, 1000);
      }, true);

      function filterProperties(location) {
        //vm.proprietiesFounded =  proprieties || vm.proprietiesFounded;

        var proprietiesFiltered = angular.copy(vm.proprietiesFounded).filter(function (propriety) {

          var accept = true;

          if (vm.filters.type) {
            switch (vm.filters.type) {
              case 'apartment':
                accept = accept && propriety.address.unit;
                break;
              case 'house':
                accept = accept && !propriety.address.unit;
            }
          }

          for (var i = 0, l = pairs.length; i<l; i++) {

            if (vm.filters[pairs[i].min]) {
              if (vm.filters[pairs[i].max]) {
                accept = accept &&
                  ((pairs[i].getValue(propriety) >= vm.filters[pairs[i].min])
                  && (pairs[i].getValue(propriety) <= vm.filters[pairs[i].max]));

                //continue;
              } else {
                accept = accept && (pairs[i].getValue(propriety) >= vm.filters[pairs[i].min]);
              }
            } else {
              if (vm.filters[pairs[i].max]) {
                accept = accept && (pairs[i].getValue(propriety) <= vm.filters[pairs[i].max]);
              }
            }
          }
          return accept;
        });

        if ((!vm.location || location)&& proprietiesFiltered.length) {
          vm.location = proprietiesFiltered[0].geo;
        }

        $rootScope.$broadcast('search', proprietiesFiltered);

        return proprietiesFiltered;
      }

      vm.selectedCity = function () {
        vm.geoSearch(vm.query.q);
      };

      vm.cancelQuery = function (filter) {
        if (filter === 'type') {
          vm.filters.type = undefined;
        }

        pairs.forEach(function (pair) {
          if (pair.min === filter || pair.max === filter) {
            vm.filters[pair.min] = undefined;
            vm.filters[pair.max] = undefined;
          }
        });

      };

      vm.getQueryLabel = function (filter, filters) {
        var targetFilter = filters || vm.filters;

        if (!targetFilter[filter]) return undefined;

        if (filter === 'type') {
          return labels[targetFilter[filter]];
        }

        for (var i = 0, l = pairs.length; i<l; i++) {

          switch (filter) {
            case pairs[i].min:
              if (targetFilter[pairs[i].max] && targetFilter[pairs[i].min]) {
                return labels[filter] + ' (Min:' + targetFilter[filter] + '-Max:' + targetFilter[pairs[i].max] + ')';
              }
              return labels[filter] + ' (Min:' + targetFilter[filter] + ')';

            case pairs[i].max:
              if (targetFilter[pairs[i].min]) return undefined;
              return labels[filter] + ' (Max:' + targetFilter[filter] + ')';
          }
        }

      };

      vm.getSearchProperties = function (filters) {
        return Object.getOwnPropertyNames(filters || vm.filters || {}).filter(function (property) {
          //TODO verificar si es necesario excluir algo.
          return [].indexOf(property) === -1;
        });
      };

      vm.homeType = { // TODO directive.
        types: [
          {
            value: 'apartment',
            label: labels['apartment']
          },
          {
            value: 'house',
            label: labels['house']
          }
        ],
        select: function (homeType) {
          vm.filters.type = vm.filters.type === homeType? undefined: homeType;
        },
        isSelected: function (homeType) {
          return vm.filters.type === homeType;
        }
      };

      vm.clearSearch = function () {
        vm.search('');
        vm.geoLocation();
      };

      vm.geoSearch = function (q) {
        GeoCoder.geocode({address: q + ', FL'})
          .then(function (results) {

            var geo = undefined;
            if (results.some(function (result) {
              //noinspection JSUnusedAssignment
              var cache = undefined;
              if (cache = result.address_components.some(function (address) {
                return address.types.indexOf('administrative_area_level_1') !== -1 && address.short_name === 'FL';
              })) {
                geo = result;
              }

              return cache;
            })) {

              vm.location = {
                lat: geo.geometry.location.lat(),
                lng: geo.geometry.location.lng()
              };

              vm.zoom = MAP_SETTINGS.cityZoom;

              vm.search(q);
            } else {
              toastr.info('Solo se pueden realizar busquedas dentro del área de Florida.');
            }

          });
      };

      function processProprieties(proprieties, location) {
        if (vm.finding) {
          Array.prototype.push.apply(vm.proprietiesFounded, proprieties);
        } else {
          vm.proprietiesFounded = proprieties;
          vm.finding = false;
        }

        filterProperties(location);
        if (proprieties.length === 500 && vm.query.q) {
          vm.finding = true;

          var query = angular.copy(vm.query);
          query.lastId = proprieties[proprieties.length - 1].mlsId;
          Propriety.query(query, function (proprieties) {
            if (vm.finding) processProprieties.bind(vm)(proprieties);
          });
        } else {
          vm.finding = false;
        }
      }

      vm.search = function (q) {
        vm.query.q = q || undefined;
        vm.finding = false;
        Propriety.query(vm.query, function (proprieties) {
          if (!proprieties.length) {
            toastr.info('No se encontraron resultados.');
            return;
          }

          if (!TokenService.has()) {
            processProprieties(proprieties);
            return;
          }

          if (vm.query.q || !!Object.keys(vm.filters).length) {
            Search.save({type: 'HISTORY', query: vm.query, filters: vm.filters}, function (search) {
              processProprieties(proprieties);
            });
          } else {
            processProprieties(proprieties);
          }

        });
      };

      vm.search('');

      vm.googleMapsUrl = googleMapsApiUrl;

      vm.showAccess = function () {
        $uibModal.open({
          windowTopClass: 'modal-primary modal-access',
          templateUrl: 'app/modules/access/access.html',
          controller: 'AccessController',
          controllerAs: 'modalVm',
          size: 'md'
        });
      };

      vm.saveSearch = function () {
        if (vm.query.q) {
          Search.save({type: 'SEARCH', query: vm.query, filters: vm.filters}, function (search) {
            toastr.info('Stored search');
          });
        }
      };

      $rootScope.$on('backSearch', function (event, query, filters) {

        vm.query = query;
        vm.filters = filters || {};
        vm.zoom = MAP_SETTINGS.cityZoom;

        Propriety.query(vm.query, function (proprieties) {
          if (!proprieties.length) return;
          processProprieties(proprieties, true);
        });
      });

      vm.currentUser = function () {
        return TokenService.get();
      };

      vm.isLogged = function () {
        return TokenService.has();
      };

      vm.logOut = function () {
        TokenService.del();
        vm.showSearches = false;
      };

      vm.showPropriety = function (propriety) {
        $uibModal.open({
          templateUrl: 'app/modules/propriety/propriety.html',
          windowTemplateUrl: 'app/modules/propriety/propriety.layout.html',
          controller: 'ProprietyController',
          controllerAs: 'modalVm',
          windowClass: 'modal-propriety',
          resolve: {
            propriety: function () {
              return propriety;
            },
            comparables: function () {
              return Propriety.query({
                limit: 10,
                lastId: 0,
                minprice: propriety.listPrice * 0.9,
                maxprice: propriety.listPrice * 1.1,
                minbaths: (propriety.property.bathsFull + propriety.property.bathsHalf) - 1,
                maxbaths: (propriety.property.bathsFull + propriety.property.bathsHalf) + 1,
                minbeds: propriety.property.bedrooms - 1,
                maxbeds: propriety.property.bedrooms + 1,
                city: propriety.address.city
              }).$promise;
            },
            state: function () {
              return vm;
            }
          }
        });
      };

      vm.changeSearchesTab = function (index) {
        vm.showSearches = true;
        $rootScope.$emit('changeTab', index);
      };

      vm.toBuy = function () {
        vm.filters.maxprice = undefined;
        vm.buy = !vm.buy;
        vm.rent = false;
        filterProperties();
      };

      vm.toRent = function () {
        vm.rent = !vm.rent;
        vm.filters.maxprice = vm.rent? 20000: undefined;
        vm.buy = false;
        filterProperties();
      };
    }
  );
