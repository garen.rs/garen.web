angular
  .module('app')
  .config(function ($stateProvider, $urlRouterProvider) {
    // $locationProvider.html5Mode(true).hashPrefix('!');
    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('anon', {
        abstract: true,
        template: '<ui-view></ui-view>'/*,
        resolve: {
          security: function ($q, TokenService) {
            if (TokenService.has()) {
              return $q.reject('NotAnon');
            }
          }
        }*/
      })
      .state('anon.main', {
        url: '/',
        templateUrl: 'app/modules/main/main.html',
        controller: 'MainController',
        controllerAs: 'vm'
      });

    $urlRouterProvider.otherwise('/');
  });

