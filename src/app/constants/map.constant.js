angular
  .module('app')
  .constant('MAP_SETTINGS', {
    defaultZoom: 10,
    cityZoom: 13
  });
