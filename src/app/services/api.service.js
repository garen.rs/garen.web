angular
  .module('app')
  .service('ApiService', function (baseUrl) {
    function toUrl(path) {
      return baseUrl + path;
    }

    this.clients = toUrl('/clients/:_id');
    this.access = toUrl('/token');
    this.searches = toUrl('/searches/:_id');
    this.proprieties = toUrl('/proprieties/:_id');
    this.schools = toUrl('/schools');

  });
