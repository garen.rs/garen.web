angular
  .module('app')
  .service('SimplyRetsApiService', function (simplyRetsUrl) {
    function toUrl(path) {
      return simplyRetsUrl + path;
    }

    this.proprieties = toUrl('/properties');
  });
