angular
  .module('app')
  .service('SimplyRetsApiInterceptorService', function ($base64, $q, $injector, simplyRetsUrl, simplyRetsCredentials) {
    return {
      request: function (config) {
        if (config.url.indexOf(simplyRetsUrl) !== -1) {
          config.headers = config.headers || {};
          config.headers['Content-Type'] = 'application/json';
          config.headers.Accept = 'application/json';
          if (simplyRetsCredentials) {
            config.headers.Authorization = 'Basic ' + $base64.encode(simplyRetsCredentials);
          }
        }
        return config;
      }
    };
  });
