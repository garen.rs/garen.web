angular
  .module('app')
  .service('TokenService', function ($window) {
    this.set = function (user) {
      $window.sessionStorage.setItem('user', JSON.stringify(user));
    };

    this.get = function () {
      return JSON.parse($window.sessionStorage.getItem('user'));
    };

    this.has = function () {
      return Boolean($window.sessionStorage.getItem('user'));
    };

    this.del = function () {
      $window.sessionStorage.removeItem('user');
    };
  });
