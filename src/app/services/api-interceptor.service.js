angular
  .module('app')
  .service('ApiInterceptorService', function ($q, $injector, baseUrl) {
    return {
      request: function (config) {
        if (config.url.indexOf(baseUrl) !== -1) {
          var TokenService = $injector.get('TokenService');
          config.headers = config.headers || {};
          config.headers['Content-Type'] = 'application/json';
          config.headers.Accept = 'application/json';
          if (TokenService.has()) {
            config.headers.Authorization = 'Bearer ' + TokenService.get().token;
          }
        }
        return config;
      }
    };
  });
