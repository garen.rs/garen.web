angular
  .module('app')
  .controller('MainController', function ($rootScope, NgMap) {
    var vm = this;


    $rootScope.$on('search', function (event, proprieties) {
//      if (reset) vm.markers = [];
      vm.markers = [];

      NgMap.getMap('map')
        .then(function (map) {
          vm.map = map;
          angular.forEach(proprieties, function (propriety) {
            var marker = new google.maps.Marker({
              position: new google.maps.LatLng(propriety.lat(), propriety.lng()),
              icon: new google.maps.MarkerImage(
                'assets/images/marker.png',
                new google.maps.Size(21, 29)
              )
            });

            marker.id = propriety.mlsId;
            marker.addListener('click', function (event) {
              vm.showDescription(event, propriety);
            });

            vm.markers.push(marker);
          });

          if (vm.markerClusterer) {
            vm.markerClusterer.clearMarkers();
            vm.markerClusterer.addMarkers(vm.markers);
          } else {
            vm.markerClusterer = new MarkerClusterer(map, vm.markers, {
              styles: [
                {
                  textColor: 'white',
                  height: 53,
                  url: 'assets/images/m1.png',
                  width: 53
                },
                {
                  textColor: 'white',
                  height: 56,
                  url: 'assets/images/m2.png',
                  width: 56
                },
                {
                  textColor: 'white',
                  height: 66,
                  url: 'assets/images/m3.png',
                  width: 66
                },
                {
                  textColor: 'white',
                  height: 78,
                  url: 'assets/images/m4.png',
                  width: 78
                },
                {
                  textColor: 'white',
                  height: 90,
                  url: 'assets/images/m5.png',
                  width: 90
                }
              ],
              maxZoom: 17
            });
          }
        })
        .catch(function (error) {
          console.log('nah ' + error);
        });
    });

    vm.showDescription = function (event, propriety) {
      vm.propriety = propriety;
      vm.map.showInfoWindow('description');
    };

  });
