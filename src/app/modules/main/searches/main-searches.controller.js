angular
  .module('app')
  .controller('MainSearchesController', function ($rootScope, Search, Propriety, $scope) {
    var vm = this;

    vm.tabActive = 2;

    $rootScope.$on('changeTab', function (event, index) {
      vm.tabActive = index;
    });

    $scope.$watch('vm.tabActive', function (a, b) {
      if (a!==b) {
        find(a);
      }
    });

    function find(index) {
      switch (index) {
        case 0:
          vm.favorites = Propriety.favs();
          break;

        case 1:
          vm.searches = Search.query({type: 'SEARCH'});
          break;
      }

    }

    $rootScope.$on('reloadTab', function (event, index) {
      find(index);
    });

    vm.removeSearch = function (index) {
      vm.searches[index].$remove();
      vm.searches.splice(index, 1);
    };

    vm.research = function (query, filters) {
      $rootScope.$emit('backSearch', query, filters);
    };

  });
