angular
  .module('app')
  .controller('MainProprietiesController', function ($rootScope) {
    var vm = this;

    vm.proprieties = [];

    $rootScope.$on('search', function (event, proprieties) {
      vm.proprieties = proprieties;
    });

  });
