angular
  .module('app')
  .controller('ProprietyController',
    function (state, $scope, $rootScope, toastr, $uibModalInstance, propriety, Propriety, comparables, School) {
      var vm = this;

      vm.propriety = propriety;

      vm.state = state;

      vm.elementarySchools = School.query({lat: propriety.geo.lat, lng: propriety.geo.lng, type: 'elementary', limit: 3});
      vm.highSchools = School.query({lat: propriety.geo.lat, lng: propriety.geo.lng, type: 'high', limit: 3});
      vm.middleSchools = School.query({lat: propriety.geo.lat, lng: propriety.geo.lng, type: 'middle', limit: 3});

      vm.comparableProprieties =  comparables;

      //TODO quitar esto...
      vm.index = $rootScope.$$childHead.state;

      vm.close = function () {
        $uibModalInstance.dismiss('cancel');
      };

      vm.savePropriety = function () {
        Propriety.fav({propriety: propriety}, function () {
          toastr.info('Property saved!');
          $rootScope.$emit('reloadTab', 0);
        });
      };

    }
  );
