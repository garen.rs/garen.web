angular
  .module('app')
  .controller('AccessController', function (toastr, $uibModalInstance, Client, TokenService) {
    var vm = this;

    vm.signUp = false;

    vm.close = function () {
      $uibModalInstance.dismiss('cancel');
    };

    vm.save = function (form) {

      if (!form.$valid) {
        toastr.error('Agrega los valores requeridos.');
        return;
      }

      if (vm.signUp && vm.client.password !== vm.client.password_confirmation) {
        toastr.error('Las contraseñas no coinciden.');
        return;
      }

      (vm.signUp?
        Client.save(vm.client):
        Client.access(vm.client)
      ).$promise
        .then(function (user) {
          if (vm.signUp) {
            vm.signUp = false;
            return;
          }

          TokenService.set(user);
          $uibModalInstance.dismiss('cancel');
        })
        .catch(function (error) {
          switch (error.data.message) {
            case 'DuplicatedKeyError':
              toastr.error('El usuario ya existe.');
              break;
            case 'InvalidUser':
              toastr.error('Usuario inválido.');
              break;
            case 'UserNotFound':
              toastr.error('Contraseña o usuario inválido.');
              break;
          }

        });

    };

  });
