angular
  .module('app')
  .factory('Client', function ($resource, ApiService) {
    return $resource(ApiService.clients, {}, {
      access: {
        url: ApiService.access,
        method: 'POST'
      }
    });
  });
