angular
  .module('app')
  .factory('Propriety', function ($resource, SimplyRetsApiService, ApiService) {
    var resource = $resource(SimplyRetsApiService.proprieties, {}, {
      fav: {
        url: ApiService.proprieties,
        method: 'POST'
      },
      favs: {
        url: ApiService.proprieties,
        method: 'GET',
        isArray: true
      }
    });

    function randomCoordinate() {
      return Math.random() * (0.0001 - 0.00005) + 0.00005;
    }

    resource.prototype.lat = function () {
      return this.geo.lat + randomCoordinate();
    };

    resource.prototype.lng = function () {
      return this.geo.lng + randomCoordinate();
    };

    return resource;
  });
