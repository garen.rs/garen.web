angular
  .module('app')
  .factory('Search', function ($resource, ApiService) {
    return $resource(ApiService.searches, {_id:'@_id'}, {});
  });
