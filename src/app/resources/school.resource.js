angular
  .module('app')
  .factory('School', function ($resource, ApiService) {
    return $resource(ApiService.schools, {}, {});
  });
