angular
  .module('app', [
    'ui.router',
    'ngResource',
    'ngMap',
    'base64',
    'ui.bootstrap',
    'toastr',
    'angular-loading-bar',
    'slick'
  ]);
